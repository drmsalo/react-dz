import "./App.css";
import React, { useEffect, useState } from "react";
import Header from "./components/Header";
import AppRoute from "./components/AppRoutes";

function App() {
  const [data, setData] = useState([]);
  const [isCart, setIsCart] = useState(false);
  const [cart, setCart] = useState([]);
  const [fav, setFav] = useState([]);
  const [article, setArticle] = useState("");

  useEffect(() => {
    const favorite = localStorage.getItem("fav");
    const cart = localStorage.getItem("cart");

    if (favorite) {
      setFav(JSON.parse(favorite));
    }

    if (cart) {
      setCart(JSON.parse(cart));
    }
    const fetchData = async () => {
      const cars = await fetch("./data.json").then((res) => res.json());
      setData(cars);
    };
    fetchData().catch(console.error);
  }, []);

  function toggleCart() {
    setIsCart((prevState) => !prevState);
  }

  function articleLoad(article) {
    setArticle(article);
    toggleCart();
  }

  function addToFav(article) {
    setFav((prevFav) => {
      const newFav = prevFav.includes(article)
        ? prevFav.filter((item) => item !== article)
        : [...prevFav, article];
      localStorage.setItem("fav", JSON.stringify(newFav));
      return newFav;
    });
  }

  function addToCart() {
    setCart((prevCart) => {
      const newCart = [...prevCart, article];
      localStorage.setItem("cart", JSON.stringify(newCart));
      return newCart;
    });
    toggleCart();
  }

  function removeCart() {
    setCart((prevCart) => {
      console.log(article);
      const newCart = prevCart.filter((item) => item !== article);
      console.log(newCart, prevCart);
      localStorage.setItem("cart", JSON.stringify(newCart));
      return newCart;
    });
    toggleCart();
  }


  return (
    <>
      <Header cartAmount={cart.length} favAmount={fav.length} />
      <AppRoute
        data={data}
        isCart={isCart}
        cart={cart}
        fav={fav}
        addToFav={addToFav}
        articleLoad={articleLoad}
        toggleCart={toggleCart}
        addToCart={addToCart}
        removeCart={removeCart}
      />
    </>
  );
}

export default App;
