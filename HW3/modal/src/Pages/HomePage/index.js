import style from "./HomePage.module.scss";
import Button from "../../components/Button";
import Modal from "../../components/Modal";
import CardsList from "../../components/CardsList";

function HomePage(props) {
  const {
    data,
    isCart,
    cart,
    fav,
    addToFav,
    articleLoad,
    toggleCart,
    addToCart,
  } = props;
  return (
    <>
      <h1 className={style.title}>Home</h1>
      <div className={style.container}>
        <CardsList
          cart={cart}
          fav={fav}
          addToFavIcon={"./img/fav-icon.png"}
          addToFav={addToFav}
          addInCart={articleLoad}
          cards={data}
        />
      </div>

      {isCart && (
        <>
          <Modal
            onClick={toggleCart}
            header={style.modal__header}
            className={style.first_modal__wrapper}
            title="Do you want to add this item to basket?"
            text="If yes this item will be added to ur basket"
            actions={
              <div className={style.pop_up_button__container}>
                <Button
                  className={style.pop_up__button}
                  onClick={addToCart}
                  text="Yes"
                />
                <Button
                  className={style.pop_up__button}
                  onClick={toggleCart}
                  text="No"
                />
              </div>
            }
          />
        </>
      )}
    </>
  );
}
export default HomePage;
