import React from "react";
import Card from "../../components/Card";
import style from "./FavPage.module.scss";

function FavoritesPage(props) {
  const { fav, cards, addToFav} = props;
  return (

    <div>
      <h1 className={style.fav__title}>Favorites</h1>
      <div className={style.container}>
      <ul className={style.fav__list}>
        {cards.map(({ name, price, url, article, color }) => {
          if (fav.includes(article)) {
            return(        
            <li className={style.favs__item} key={article}>
             <Card name={name} price={price} url={url} addToFavIcon="./img/star-icon.png" color={color} onClick={() =>addToFav(article)}/> 
             </li> 
            )
          }
        })}
        
      </ul>
      </div>
    </div>
  );
}
export default FavoritesPage;