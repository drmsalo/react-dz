import React from "react";
import Button from "../../components/Button";
import Card from "../../components/Card";
import Modal from "../../components/Modal";
import style from "./CartPage.module.scss";

function Cart(props) {
  const { cart, toggleCart, data, removeCart, articleLoad, isCart } = props;

  return (
    <>
      <h1 className={style.title}>Cart Page</h1>
      <div className={style.container}>
        <ul className={style.cart__list}>
          {data.map(({ name, price, url, article, color }) => {
            if (cart.includes(article)) {
              return (
                <li className={style.cart__item} key={article}>
                  <Card
                    name={name}
                    price={price}
                    url={url}
                    addToFavIcon="./img/star-icon.png"
                    actions={
                      <button onClick={() => articleLoad(article)}>
                        Remove
                      </button>
                    }
                    color={color}
                  />
                </li>
              );
            }
          })}
        </ul>
      </div>

      {isCart && (
        <Modal
          onClick={toggleCart}
          header={style.modal__header}
          className={style.first_modal__wrapper}
          title="Do you want to delete this item from your basket?"
          text="If yes this item will be deleted from ur basket"
          actions={
            <div className={style.pop_up_button__container}>
              <Button
                className={style.pop_up__button}
                onClick={() => {
                  removeCart(articleLoad);
                }}
                text="Yes"
              />
              <Button
                className={style.pop_up__button}
                onClick={toggleCart}
                text="No"
              />
            </div>
          }
        />
      )}
    </>
  );
}
export default Cart;
