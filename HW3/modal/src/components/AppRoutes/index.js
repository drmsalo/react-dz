import React from "react";
import { Routes, Route } from "react-router-dom";
import Cart from "../../Pages/CartPage";
import FavoritesPage from "../../Pages/FavoritePage";

import HomePage from "../../Pages/HomePage";



function AppRoute(props) {
  const {
    data,
    isCart,
    isFav,
    cart,
    setCart,
    removeCart,
    setArticle,
    toggleFav,
    fav,
    addToFav,
    articleLoad,
    toggleCart,
    addToCart,
  } = props;
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <HomePage
              data={data}
              isCart={isCart}
              cart={cart}
              fav={fav}
              addToFav={addToFav}
              articleLoad={articleLoad}
              toggleCart={toggleCart}
              addToCart={addToCart}
            />
          }
        />
        <Route
          path="/in-cart"
          element={
            <Cart
              cart={cart}
              setCart={setCart}
              toggleCart={toggleCart}
              setArticle={setArticle}
              data={data}
              articleLoad={articleLoad}
              removeCart={removeCart}
              isCart={isCart}
            />
          }
        />
        <Route 
        path="/in-favorites"
        element={
          <FavoritesPage isFav={isFav} toggleFav={toggleFav} addToFav={addToFav} cards={data} fav={fav}/>
        }/>
  </Routes>
        
    </>
  );
}

export default AppRoute;
