import React from "react";

function Button (props){
    const {style,onClick, text, className} = props
    return (
      <div>
        <button style={style}className={className} onClick={onClick}>{text}</button>
      </div>
    );

}
export default Button;
