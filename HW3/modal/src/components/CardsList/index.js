import React from "react";
import Button from "../Button";
import Card from "../Card";
import style from "./CardsList.module.scss";

function CardsList(props) {
  const { cards, addInCart, addToFav, addToFavIcon, fav, cart} = props;

  return (
    <ul className={style.cards__list}>
      {cards.map(({ name, price, article, url, color }) => (
        <li className={style.cards__item} key={article}>
          <Card
            isCart={cart.includes(article)}
            isFav={fav.includes(article)}
            addToFavIcon={addToFavIcon}
            url={url}
            article={article}
            name={name}
            price={price}
            color={color}
            onClick={() => addToFav(article)}
            actions={
              <>
                <Button
                  onClick={() => addInCart(article)}
                  className={style.add__button}
                  text="Add to Cart"
                />
              </>
            }
          />
        </li>
      ))}
    </ul>
  );
}

export default CardsList;
