import React from "react";
import style from "./Card.module.scss";

function Card(props) {
  
    const { name, price, url, article, color, actions, addToFavIcon, onClick, isFav} = props

    return (
      <div id={article}>
        <img width="350px" height="250px" alt="Car Model" src={url} />
        <img
          onClick={onClick}
          width="30px"
          height="30px"
          alt="Add to Favourites"
        
        src={isFav ? "./img/star-icon.png" : addToFavIcon}
        />
        
        <h2>{name}</h2>
        <p className={style.price}>
          {price.toLocaleString()} <span> €</span>
        </p>
        <div className={style.card__footer}>
          <div
            className={style.circle__icon}
            style={{ background: color }}
          ></div>
          {actions}
        </div>
      </div>
    );
}
export default Card;
