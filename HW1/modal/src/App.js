import "./App.css";
import style from "../src/App.module.scss";
import React, { Component } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  };
  handleOpenModalFirst = () => {
    this.setState((prevState) => ({
      isFirstModalOpen: !prevState.isFirstModalOpen,
    }));
  };

  handleOpenModalSecond = () => {
    this.setState((prevState) => ({
      isSecondModalOpen: !prevState.isSecondModalOpen,
    }));
  };

  render() {
    const { isFirstModalOpen, isSecondModalOpen } = this.state;
    return (
      <>
      <div className={style.button__container}>
        <Button
          className={style.modal__button_first}
          text="Open First Modal"
          onClick={this.handleOpenModalFirst}
        />
        <Button
          className={style.modal__button_second}
          text="Open Second Modal"
          onClick={this.handleOpenModalSecond}
        />
        </div>
      
        {isFirstModalOpen && (
          <>
          <Modal
          onClick={this.handleOpenModalFirst}
          header={style.modal__header}
            className={style.first_modal__wrapper}
            src="./img/mercedes.jpg"
            title="Do you want to delete this file?"
            text="Once you delete this file, it won't be possible to undo this action"
            warning="Are you sure you want to delete it?"
            actions={
              <div className={style.pop_up_button__container}>
                <Button className={style.pop_up__button} onClick={this.handleOpenModalFirst} text="Ok" />
                <Button  className={style.pop_up__button} onClick={this.handleOpenModalFirst} text="Cancel"/>
              </div>
            }
          />
          </>
        )}
          
        
        {isSecondModalOpen && (
          <Modal
          onClick={this.handleOpenModalSecond}
          header={style.modal__header}
          className={style.second_modal__wrapper}
          src="./img/audi.jpg"
          title="Do u want to save this file?"
          text="Once you Save this file, it won't be possible to undo this action"
          warning="Are you sure you want to save it?"
          actions={
            <div  className={style.pop_up_button__container}>
              <Button className={style.pop_up__button} text="Save" onClick={this.handleOpenModalSecond}/>
                <Button style={{backgroundColor: "red"}} className={style.pop_up__button} text="Cancel"onClick={this.handleOpenModalSecond}/>
            </div>
          }
          />
        )}
    </>  
    );
  }
}

export default App;
