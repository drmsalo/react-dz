import React, { Component } from "react";

class Button extends Component {
    
  render() {
    const {style,onClick, text, className} = this.props
    return (
      <div>
        <button style={style}className={className} onClick={onClick}>{text}</button>
      </div>
    );
  }
}
export default Button;
