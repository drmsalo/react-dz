import React, { Component } from "react";
import style from "./Modal.module.scss"

class Modal extends Component{
    render(){
        const {title,text, warning ,className, src, closeButton, actions, header, onClick} = this.props
    return(
        <>
        <div className={className}>
        <header className={header}>
            <h1 className={style.modal__title}>
                {title}
            </h1>
            </header>
            <img className={style.modal__img}  alt="img" src={src}/>
        <div className={style.text__container}>
        <p>{text}</p>
        <p>{warning}</p>
        </div>
        {closeButton}
        {actions}
        </div>
        <div onClick={onClick} className={style.modal__bg}>

        </div>
        </>
    )
}
}

export default Modal