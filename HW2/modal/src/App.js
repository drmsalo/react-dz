import "./App.css";
// import style from "../src/App.module.scss";
import React from "react";
import HomePage from "./Pages/HomePage";
import { Route, Routes } from "react-router-dom";
// import Button from "./components/Button";
// import Modal from "./components/Modal";
// import Header from "./components/Header";
// import CardsList from "./components/CardsList";
import Cart from "./Pages/CartPage";
import Favorites from "./Pages/FavoritePage";

function App() {
  
  // const [data, setData] = useState([]);
  // const [isCart, setIsCart] = useState(false);
  // const [cart, setCart] = useState([]);
  // const [fav, setFav] = useState([]);
  // const [article, setArticle] = useState(0);

  // useEffect(() => {
  //   const favorite = localStorage.getItem("fav");
  //   const cart = localStorage.getItem("cart");

  //   if (favorite) {
  //     setFav(JSON.parse(favorite));
  //   }
  //   if (cart) {
  //     setCart(JSON.parse(cart) );
  //   }
  //   const fetchData = async () => {
  //     const cars = await fetch("./data.json").then((res) => res.json());
  //     setData(cars)
  //   };
  //   fetchData()
  //   .catch(console.error);
  // },[data]);

  // function toggleCart () {
  //   setIsCart((prevState) => !prevState)  
  

  // };

  // function articleLoad  (article)  {
  //   setArticle( article );
  //   toggleCart();
  // };

  // function addToFav (article) {
  //   if (fav.includes(article)) {
  //     setFav(fav.splice(fav.indexOf(article), 1));
  //   } else {
  //     setFav(
  //       fav.push(article));
  //   }
  //   setFav(fav);
  //   localStorage.setItem("fav", JSON.stringify(fav));
  // };

  // function addToCart () {
  //   const art = article;
  //   setCart(cart.push(art));
  //   setCart(cart);
  //   localStorage.setItem("cart", JSON.stringify(cart));
  //   toggleCart();
  // };


  return (
    
    <>
      <Routes>
        <Route path="/" element={<HomePage/>}/>
          <Route path ="/basket" element={<Cart/>}/>
          <Route path="/favorites" element={<Favorites/>}/>
        </Routes>
      
      {/* <Header favAmount={fav.length} cartAmount={cart.length} />
      <div className={style.container}>
        <CardsList
          cart={cart}
          fav={fav}
          addToFavIcon={"./img/fav-icon.png"}
          addToFav={addToFav}
          addInCart={articleLoad}
          cards={data}
        />
        <Routes>
        <Route path ="/basket" element={<Cart/>}/>
        <Route path="/favorites" element={<Favorites/>}/>
      </Routes>
      </div>

      {isCart && (
        <>
          <Modal
            onClick={toggleCart}
            header={style.modal__header}
            className={style.first_modal__wrapper}
            title="Do you want to add this item to basket?"
            text="If yes this item will be added to ur basket"
            actions={
              <div className={style.pop_up_button__container}>
                <Button
                  className={style.pop_up__button}
                  onClick={addToCart}
                  text="Yes"
                />
                <Button
                  className={style.pop_up__button}
                  onClick={toggleCart}
                  text="No"
                />
              </div>
            }
          />
        </>
      )}
      */}
    </> 
  );
}

export default App;
