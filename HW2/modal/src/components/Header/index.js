import React from "react";
import { Link } from "react-router-dom";
import style from "./Header.module.scss";

function Header(props){
  
    const { favAmount, cartAmount} = props;
    return (
      <header className={style.header__page}>
        <Link className={style.nav__link} to={"/"}>HomePage</Link>
        <Link className={style.nav__link} to={"/favorites"}>Favorites</Link>
        <Link className={style.nav__link} to={"/basket"}>Cart</Link>
        
        
        <div className={style.icons__container}>
          <div className={style.icon__wrapper}>
            <img
              className={style.star__icon}
              alt="fav"
              src="../../img/star-icon.png"
            />
           <span className={style.favs__ammount}>{favAmount}</span>
          </div>
          <div className={style.icon__wrapper}>
            <img
              className={style.car__icon}
              alt="cart"
              src="../../img/car-icon.png"
            />
            <span className={style.cart__ammount}>{cartAmount}</span>
          </div>
        </div>
      </header>
    );
  }


export default Header;
