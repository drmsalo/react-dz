import React from "react";
import Cards from "./Cards";
import { useFunction } from "../hook/useFunction";

export default function HomePage() {
  const { likedItems } = useFunction();
  const { unlikedProduct } = useFunction();
  const { likedProduct } = useFunction();

  return (
    <>
      <div>
        <Cards
          likedItems={likedItems}
          likedProduct={likedProduct}
          unlikedProduct={unlikedProduct}
        />
      </div>
    </>
  );
}
